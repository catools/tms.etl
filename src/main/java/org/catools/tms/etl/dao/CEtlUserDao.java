package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlUser;
import org.slf4j.Logger;

public class CEtlUserDao extends CEtlBaseDao {
    public static void mergeUser(Logger logger, CEtlUser user) {
        merge(logger, user);
    }

    public static CEtlUser getUserByName(Logger logger, String name) {
        return find(logger, CEtlUser.class, name);
    }
}
