package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlProject;
import org.catools.tms.etl.model.CEtlProjects;
import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;

public class CEtlProjectDao extends CEtlBaseDao {
    public static void mergeProject(Logger logger, CEtlProject project) {
        persist(logger, project);
    }

    public static CEtlProject getProjectById(Logger logger, String id) {
        return find(logger, CEtlProject.class, id);
    }

    public static CEtlProjects getProjects(Logger logger) {
        return getTransactionResult(logger, session -> {
            return new CEtlProjects(session.createQuery("From CEtlProject", CEtlProject.class).setHint(QueryHints.CACHEABLE, true).getResultList());
        });
    }
}
