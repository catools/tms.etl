package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlProject;
import org.catools.tms.etl.model.CEtlVersion;
import org.catools.tms.etl.model.CEtlVersions;
import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;

public class CEtlVersionDao extends CEtlBaseDao {
    public static void mergeVersion(Logger logger, CEtlVersion version) {
        merge(logger, version);
    }

    public static void mergeVersions(Logger logger, CEtlVersions versions) {
        merge(logger, versions);
    }

    public static CEtlVersion getVersionById(Logger logger, String id) {
        return find(logger, CEtlVersion.class, id);
    }

    public static CEtlVersion getVersion(Logger logger, CEtlProject project, String versionName) {
        return getTransactionResult(logger, session -> {
            return session.createNamedQuery("getVersionForProjectAndName", CEtlVersion.class)
                    .setParameter("name", versionName)
                    .setParameter("projectId", project.getId())
                    .setHint(QueryHints.CACHEABLE, true)
                    .getResultStream()
                    .findFirst()
                    .orElse(null);
        });
    }
}
