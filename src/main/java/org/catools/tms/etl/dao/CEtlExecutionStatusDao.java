package org.catools.tms.etl.dao;

import org.catools.tms.etl.model.CEtlExecutionStatus;
import org.hibernate.annotations.QueryHints;
import org.slf4j.Logger;

public class CEtlExecutionStatusDao extends CEtlBaseDao {
    public static CEtlExecutionStatus getStatusById(Logger logger, String id) {
        return find(logger, CEtlExecutionStatus.class, id);
    }

    public static void mergeStatus(Logger logger, CEtlExecutionStatus status) {
        merge(logger, status);
    }

    public static CEtlExecutionStatus getStatusByName(Logger logger, String name) {
        return getTransactionResult(logger, session -> {
            return session.createNamedQuery("getExecutionStatusByName", CEtlExecutionStatus.class)
                    .setParameter("name", name)
                    .setHint(QueryHints.CACHEABLE, true)
                    .getResultStream()
                    .findFirst()
                    .orElse(null);
        });
    }
}
