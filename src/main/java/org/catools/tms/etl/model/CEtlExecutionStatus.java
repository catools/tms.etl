package org.catools.tms.etl.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries({@NamedQuery(name = "getExecutionStatusByName", query = "FROM CEtlExecutionStatus where name=:name")})
@Entity
@Table(name = "execution_status", schema = "etl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "execution_status")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CEtlExecutionStatus implements Serializable {

    private static final long serialVersionUID = 6867874018185613707L;

    @Id
    @Column(name = "id", length = 20, unique = true, nullable = false)
    private String id;

    @Column(name = "name", length = 100, unique = true, nullable = false)
    private String name;
}
