package org.catools.tms.etl.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@NamedQueries({@NamedQuery(name = "getProjectById", query = "FROM CEtlProject where id=:id"), @NamedQuery(name = "getProjects", query = "FROM CEtlProject")})
@Entity
@Table(name = "project", schema = "etl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "project")
@Data
@NoArgsConstructor
@AllArgsConstructor
@Accessors(chain = true)
public class CEtlProject implements Serializable {

    private static final long serialVersionUID = 6069874018185613707L;

    @Id
    @Column(name = "id", length = 20, unique = true, nullable = false)
    private String id;

    @Column(name = "name", length = 250, unique = true, nullable = false)
    private String name;
}
