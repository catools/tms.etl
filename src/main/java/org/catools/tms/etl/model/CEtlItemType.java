package org.catools.tms.etl.model;

import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.experimental.Accessors;
import org.catools.common.utils.CStringUtil;
import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

import javax.persistence.*;
import java.io.Serializable;

@Entity
@Table(name = "item_type", schema = "etl")
@Cache(usage = CacheConcurrencyStrategy.READ_WRITE, region = "itemtype")
@Data
@NoArgsConstructor
@Accessors(chain = true)
public class CEtlItemType implements Serializable {

    private static final long serialVersionUID = 6067871108185613707L;

    @Id
    @Column(name = "id", length = 20, unique = true, nullable = false)
    private String id;

    @Column(name = "name", length = 250, unique = true, nullable = false)
    private String name;

    @Column(name = "description", length = 1000, unique = true, nullable = false)
    private String description;

    @ManyToOne(cascade = CascadeType.MERGE, targetEntity = CEtlProject.class)
    @JoinColumn(name = "project_id", referencedColumnName = "id", nullable = false)
    private CEtlProject project;

    public CEtlItemType(String id, String name, String description, CEtlProject project) {
        this.id = id;
        this.name = CStringUtil.substring(name, 0, 250);
        this.description = CStringUtil.substring(description, 0, 1000);
        this.project = project;
    }
}
